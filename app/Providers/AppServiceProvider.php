<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\Shared\CurrencyRate\Services\CurrencyRateServiceInterface;
use App\Modules\Shared\CurrencyRate\Services\CurrencyRateService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(CurrencyRateServiceInterface::class, CurrencyRateService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
