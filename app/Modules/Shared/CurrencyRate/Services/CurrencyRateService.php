<?php

namespace App\Modules\Shared\CurrencyRate\Services;

use App\Modules\Shared\CurrencyRate\Enums\CurrencyEnum;
use Illuminate\Support\Facades\Http;
use App\Models\CurrencyRate;
use Carbon\Carbon;

class CurrencyRateService implements CurrencyRateServiceInterface
{
    protected $cachedRates = [];

    private $currencyCode;
    private $exchangeRate;
    private $date;

    public function syncToDb(): void
    {
        $existingRecord = CurrencyRate::where('currency_code', $this->currencyCode)
            ->where('date', $this->date)
            ->first();

        if (!$existingRecord) {
            $currencyRate = new CurrencyRate();
            $currencyRate['currency_code'] = $this->currencyCode;
            $currencyRate['exchange_rate'] = $this->exchangeRate;
            $currencyRate['date'] = $this->date;

            $currencyRate->save();
        }
    }

    protected function fetchCurrencyRates(string $url): array
    {
        $endpoints = [
            'daily' => 'https://www.nbkr.kg/XML/daily.xml',
            'weekly' => 'https://www.nbkr.kg/XML/weekly.xml',
        ];

        $endpoint = $endpoints[$url];

        if (!isset($this->cachedRates[$url])) {
            $response = Http::get($endpoint);
            $xmlData = $response->getBody();
            $xmlObject = simplexml_load_string($xmlData);
            $jsonFormatData = json_encode($xmlObject);
            $this->cachedRates[$url] = json_decode($jsonFormatData, true);
        }

        return $this->cachedRates[$url];
    }

    public function retrieveCurrencyRate(CurrencyEnum $currency, string $url): mixed
    {
        $parsedData = $this->fetchCurrencyRates($url);
        $currencyData = $parsedData['Currency'];
        $date = $parsedData['@attributes']['Date'];
        $this->date = Carbon::createFromFormat('d.m.Y', $date)->format('Y-m-d');

        foreach ($currencyData as $currencyItem) {
            if ($currencyItem['@attributes']['ISOCode'] === $currency->value) {
                $nominal = intval($currencyItem['Nominal']);
                $rateForNominal = floatval(str_replace(',', '.', $currencyItem['Value']));
                $rate = $rateForNominal / $nominal;
                $this->exchangeRate = $rate;
                $this->currencyCode = $currency;
                $this->syncToDb();
                break;
            }
        }

        return $rate ?? null;
    }

    public function getCurrentRate(CurrencyEnum $currency): float
    {
        if ($currency->value === 'KGS') {
            return 1;
        }

        $rate = $this->retrieveCurrencyRate($currency, 'daily');

        if (!isset($rate)) {
            $rate = $this->retrieveCurrencyRate($currency, 'weekly');
        }

        if (!isset($rate)) {
            throw new \RuntimeException($currency->value . " => Rate not found");
        }

        return $rate;
    }
}
