<?php

namespace Tests\Unit;

use App\Modules\Shared\CurrencyRate\Services\CurrencyRateService;
use App\Modules\Shared\CurrencyRate\Enums\CurrencyEnum;
use Tests\TestCase;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CurrencyRateServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testReturnsCorrectKGSCurrencyRate()
    {
        $currencyRateService = new CurrencyRateService();

        $xmlData = file_get_contents(__DIR__ . '/../xml_data/daily_currency_rates.xml');

        Http::shouldReceive('get')->andReturn(new Response(200, [], $xmlData));

        $currencyEnum = CurrencyEnum::KGS;
        $rate = $currencyRateService->getCurrentRate($currencyEnum);

        $this->assertEquals($rate, 1);
    }

    public function testReturnsCorrectDailyCurrencyRate()
    {
        $currencyRateService = new CurrencyRateService();

        $xmlData = file_get_contents(__DIR__ . '/../xml_data/daily_currency_rates.xml');

        Http::shouldReceive('get')->andReturn(new Response(200, [], $xmlData));

        $currencyEnum = CurrencyEnum::USD;
        $rate = $currencyRateService->getCurrentRate($currencyEnum);

        $this->assertEquals($rate, 87.7708);

        $this->assertDatabaseHas('currency_rates', [
            'currency_code' => 'USD', 'exchange_rate' => 87.7708, 'date' => date('04.08.2023')
        ]);
    }

    public function testReturnsCorrectWeeklyCurrencyRate()
    {
        $currencyRateService = new CurrencyRateService();

        $xmlDailyData = file_get_contents(__DIR__ . '/../xml_data/daily_currency_rates.xml');
        $xmlWeeklyData = file_get_contents(__DIR__ . '/../xml_data/weekly_currency_rates.xml');

        Http::shouldReceive('get')->with('https://www.nbkr.kg/XML/daily.xml')->andReturn(new Response(200, [], $xmlDailyData));   // phpcs:ignore
        Http::shouldReceive('get')->with('https://www.nbkr.kg/XML/weekly.xml')->andReturn(new Response(200, [], $xmlWeeklyData));   // phpcs:ignore

        $currencyEnum = CurrencyEnum::CNY;
        $rate = $currencyRateService->getCurrentRate($currencyEnum);

        $this->assertEquals($rate, 12.242);

        $this->assertDatabaseHas('currency_rates', [
            'currency_code' => 'CNY', 'exchange_rate' => 12.242, 'date' => date('29.07.2023')
        ]);
    }

    public function testThrowsExceptionIfCurrencyIsMissing()
    {
        $currencyRateService = new CurrencyRateService();

        $xmlDailyData = file_get_contents(__DIR__ . '/../xml_data/daily_currency_rates.xml');
        $xmlWeeklyData = file_get_contents(__DIR__ . '/../xml_data/weekly_currency_rates.xml');

        Http::shouldReceive('get')->with('https://www.nbkr.kg/XML/daily.xml')->andReturn(new Response(200, [], $xmlDailyData));   // phpcs:ignore
        Http::shouldReceive('get')->with('https://www.nbkr.kg/XML/weekly.xml')->andReturn(new Response(200, [], $xmlWeeklyData));   // phpcs:ignore

        $currencyEnum = CurrencyEnum::KGHS;

        $this->expectException(\RuntimeException::class);

        $rate = $currencyRateService->getCurrentRate($currencyEnum);
    }
}
