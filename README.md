### О проекте

Проект был выполнен [Артёмом Похилюком](https://www.linkedin.com/in/artem-pokhiliuk/) как тестовое задание. Он представляет собой парсер валют с сайта Национального банка Кыргызской республики, реализованный как консольная команда Laravel:

  php artisan app:parse-currency-rates

Полученные данные сохраняются в базу данных PostgreSQL. Код покрыт тестами с использованием моков.

### Исходное задание

Необходимо реализовать сервисный класс для парсинга курсов валют с сайта https://www.nbkr.kg, имплементирующий интерфейс
`App\Modules\Shared\CurrencyRate\Services\CurrencyRateServiceInterface`.

Результатом работы является работоспособность команды `App\Console\Commands\ParseCurrencyRatesCommand`, которая должны 
вывести в консоль перечень валют содержащихся в `CurrencyEnum` с текущим значением курса валюты, полученным путем
парсинга.

**!!! ВНЕСЕНИЕ ИЗМЕНЕНИЙ В ИНТЕРФЕЙС `App\Modules\Shared\CurrencyRate\Services\CurrencyRateServiceInterface` НЕ ДОПУСКАЕТСЯ !!!**

**!!! ВНЕСЕНИЕ ИЗМЕНЕНИЙ В КОД КОМАНДЫ `App\Console\Commands\ParseCurrencyRatesCommand` НЕ ДОПУСКАЕТСЯ !!!**

Пример вывода:

```shell
> php artisan app:parse-currency-rates

"KGS => 1" // app/Console/Commands/ParseCurrencyRatesCommand.php:31
"RUB => 0.9658" // app/Console/Commands/ParseCurrencyRatesCommand.php:31
"USD => 87.87" // app/Console/Commands/ParseCurrencyRatesCommand.php:31
"EUR => 98.4803" // app/Console/Commands/ParseCurrencyRatesCommand.php:31
"KZT => 0.1979" // app/Console/Commands/ParseCurrencyRatesCommand.php:31
"CNY => 12.3129" // app/Console/Commands/ParseCurrencyRatesCommand.php:31

   RuntimeException 

  KGHS => Rate not found

```

***

Сайтом https://www.nbkr.kg предоставляется 2 набора данных по курсам валют:

курсы валют на 1 день - https://www.nbkr.kg/XML/daily.xml

курсы валют на 1 неделю - https://www.nbkr.kg/XML/weekly.xml


- Одна валюта теоретически может содержаться в обеих выгрузках. Данные из ежедневной выгрузки являются более
приоритетными (в плане значений) чем из еженедельной.
- Локальной валютой для реализации данного задания является KGS (Киргизский сом). При реализации необходимо учесть,
что данная валюта отсутствует в выгрузках и ее курс всегда равен 1.
- Необходимо учесть что для каких-то валют в выгрузке возможно указание номинала отличного от 1, например 100, что 
означает соотношение обменного курса 1 Сом за 100 единиц выбранной валюты.
- Необходимо реализовать хранение исторических данных полученных курсов валют в БД с указанием даты, на которую курс
был получен.
- Необходимо предусмотреть вариант и выброс исключения, когда запрошенная валюта отсутствует в выгрузке.

Использование сторонних пакетов в разумных пределах не возбраняется.

**Обязательно:**
- Код должен работать на PHP 8.1 и выше.
- Покрытие кода сервиса тестами, замокать внешний сервис (данные, получаемые с внешнего сайта с курсами валют).
- Описание логики работы сервиса.

### Описание логики работы сервиса

Пошагово опишу логику работу основного класса `App\Modules\Shared\CurrencyRate\Services\CurrencyRateService`, определяющую актуальный курс принимаемой в качестве параметра валюты из списка `CurrencyEnum`:

1. Если получен **"KGS"**, то сразу выплёвываем **1**.

2. Если иная валюта, то смотрим, есть ли у нас в кэше массив запроса к эндпойнту ежедневной выгрузки, и если нету, то делаем request и сохраняем response в виде массива, удобного для последующего парсинга. Если же в кэше есть уже этот массив то просто передаем его дальше для следующего шага.

3. Проходимся по массиву ежедневной выгрузки, ищем совпадение по валюте, вытаскиваем значение курса, в случае если номинал не единица, то производим деление на номинал, чтобы получить курс за 1 единицу данной валюты. Возвращаем полученный курс либо **null** если нашей валюты в ежедневной выгрузке не оказалось. Сохраняем в БД (если не **null**), при условии что там еще нет записи за эту дату.

4. Если нашей валюты в ежедневной выгрузке не оказалось, то выполняем шаги 2 и 3 но теперь уже обращаясь к эндпойнту еженедельной выгрузки. Если оказалась, то возвращаем полученное значение курса.

5. Если и в еженедельной выгрузке нет нашей валюты то выбрасываем исключение. Если оказалась - возвращаем значение курса.
